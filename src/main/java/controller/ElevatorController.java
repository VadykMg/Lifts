package controller;

import model.Elevator;
import states.ElevatorState;
import util.PropertiesUtils;


import java.util.*;

public class ElevatorController {
    private List<Elevator> allElevators = new ArrayList<>();

    private static final int MIN_FLOOR = Integer.parseInt(PropertiesUtils.getProperties().getProperty("MIN_FLOOR"));
    private static final int MAX_FLOOR = Integer.parseInt(PropertiesUtils.getProperties().getProperty("MAX_FLOOR"));

    public int up(int floor) {
        if (floor < MAX_FLOOR) {
            List<Elevator> firstPriority = getFirstPriorityForUp(floor);

            if (!firstPriority.isEmpty()) {
                return check(firstPriority, floor).getId();
            }

            List<Elevator> secondPriority = getSecondPriority(floor);
            if (!secondPriority.isEmpty()) {
                return check(secondPriority, floor).getId();
            }

            List<Elevator> thirdPriority = getThirdPriority();
            if (!thirdPriority.isEmpty()) {
                return check(thirdPriority, floor).getId();
            }

            List<Elevator> fourthPriority = getFourthPriority();
            if (!fourthPriority.isEmpty()) {
                return check(firstPriority, floor).getId();
            }

        }

        throw new IllegalArgumentException("Неможливо рухатися вгору");
    }

    public int down(int floor) {
        if (floor > MIN_FLOOR) {
            List<Elevator> firstPriority = getFirstPriorityForDown(floor);

            if (!firstPriority.isEmpty()) {
                return check(firstPriority, floor).getId();
            }

            List<Elevator> secondPriority = getSecondPriority(floor);
            if (!secondPriority.isEmpty()) {
                return check(secondPriority, floor).getId();
            }

            List<Elevator> thirdPriority = getThirdPriority();
            if (!thirdPriority.isEmpty()) {
                return check(thirdPriority, floor).getId();
            }

            List<Elevator> fourthPriority = getFourthPriority();
            if (!fourthPriority.isEmpty()) {
                return check(firstPriority, floor).getId();
            }
        }
        throw new IllegalArgumentException("Неможливо рухатися вниз");
    }

    private List<Elevator> getFourthPriority() {
        List<Elevator> fourthPriority = new ArrayList<>();
        for (Elevator elevator : allElevators) {
            if (elevator.getState() == ElevatorState.EMPTY) {
                fourthPriority.add(elevator);
            }
        }
        return fourthPriority;
    }

    private List<Elevator> getThirdPriority() {
        List<Elevator> thirdPriority = new ArrayList<>();
        for (Elevator elevator : allElevators) {
            if ((elevator.getState() != ElevatorState.EMPTY)) {
                thirdPriority.add(elevator);

            }
        }
        return thirdPriority;
    }

    private List<Elevator> getSecondPriority(int floor) {
        List<Elevator> secondPriority = new ArrayList<>();
        for (Elevator elevator : allElevators) {
            if (elevator.getState() != ElevatorState.EMPTY && elevator.getEndFloor() == floor) {
                secondPriority.add(elevator);
            }
        }
        return secondPriority;
    }


    private List<Elevator> getFirstPriorityForDown(int floor) {
        List<Elevator> firstPriority = new ArrayList<>();
        for (Elevator elevator : allElevators) {
            if (elevator.getState() == ElevatorState.DOWN && elevator.getEndFloor() <= floor && elevator.getStartFloor() > floor) {
                firstPriority.add(elevator);
            }
        }
        return firstPriority;
    }

    private List<Elevator> getFirstPriorityForUp(int floor) {
        List<Elevator> firstPriority = new ArrayList<>();
        for (Elevator elevator : allElevators) {
            if (elevator.getState() == ElevatorState.UP && elevator.getEndFloor() >= floor && elevator.getStartFloor() < floor) {
                firstPriority.add(elevator);
            }
        }
        return firstPriority;
    }


    private Elevator check(List<Elevator> list, int floor) {
        Map<Integer, Elevator> liftMap = new HashMap<>();
        int minCount = 100;
        for (Elevator elevator : list) {
            liftMap.put(Math.abs(elevator.getStartFloor() - floor), elevator);
            if (Math.abs(elevator.getStartFloor() - floor) < minCount) {
                minCount = Math.abs(elevator.getStartFloor() - floor);
            }
        }
        return liftMap.get(minCount);
    }

    public List<Elevator> getLifts() {
        return allElevators;
    }

    public void setList(List<Elevator> allElevators) {
        this.allElevators = allElevators;
    }
}
