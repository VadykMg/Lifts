package model;

import states.ElevatorState;

public class Elevator {

    private int id;
    private ElevatorState state;
    private int startFloor;
    private int endFloor;

    public Elevator() {
    }

    public Elevator(int id, ElevatorState state, int startFloor, int endFloor) {
        this.id = id;
        this.state = state;
        this.startFloor = startFloor;
        this.endFloor = endFloor;
    }

    public ElevatorState getState() {
        return state;
    }

    public void setState(ElevatorState state) {
        this.state = state;
    }

    public int getStartFloor() {
        return startFloor;
    }


    public void setStartFloor(int startFloor) {
        this.startFloor = startFloor;
    }

    public int getEndFloor() {
        return endFloor;
    }

    public void setEndFloor(int endFloor) {
        this.endFloor = endFloor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
