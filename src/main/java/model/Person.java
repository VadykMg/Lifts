package model;
import states.PersonDirection;


public class Person {
    private int personFloor;
    private PersonDirection direction;

    public Person() {

    }

    public Person(int personFloor, PersonDirection direction) {
        this.personFloor = personFloor;
        this.direction = direction;
    }


    public int getPersonFloor() {
        return personFloor;
    }

    public void setPersonFloor(int personFloor) {
        this.personFloor = personFloor;
    }

    public PersonDirection getDirection() {
        return direction;
    }

    public void setDirection(PersonDirection direction) {
        this.direction = direction;
    }
}
