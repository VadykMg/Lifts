package states;

public enum ElevatorState {
    EMPTY, UP, DOWN
}
