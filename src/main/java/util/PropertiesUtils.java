package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtils {
    private static final String fileName = "src/main/resources/application.properties";
    private PropertiesUtils() {

    }
    public static Properties getProperties() {
        Properties properties = new Properties();
        try(FileInputStream fileInputStream = new FileInputStream(fileName)) {
            properties.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
