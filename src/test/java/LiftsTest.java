import controller.ElevatorController;
import model.Elevator;
import model.Person;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import states.ElevatorState;
import states.PersonDirection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static states.PersonDirection.DOWN;
import static states.PersonDirection.UP;

public class LiftsTest extends Assert {

    private ElevatorController controller;
    private static final Logger log = Logger.getLogger(LiftsTest.class);

    @BeforeClass
    public void init() {
        controller = new ElevatorController();
    }

    @DataProvider
    public Object[][] getOptimal() {
        return new Object[][]{
                {new Person(3, UP), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.DOWN, 100, 2),
                        new Elevator(2, ElevatorState.UP, 1, 2))), 2},
                {new Person(4, PersonDirection.DOWN), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.DOWN, 100, 2),
                        new Elevator(2, ElevatorState.DOWN, 5, 3),
                        new Elevator(3, ElevatorState.DOWN, 7, 4))), 2},
                {new Person(4, PersonDirection.DOWN), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.DOWN, 100, 2),
                        new Elevator(2, ElevatorState.DOWN, 5, 4),
                        new Elevator(3, ElevatorState.UP, 2, 4))), 2},
                {new Person(96, PersonDirection.DOWN), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.DOWN, 100, 99),
                        new Elevator(2, ElevatorState.UP, 2, 96),
                        new Elevator(3, ElevatorState.DOWN, 95, 94),
                        new Elevator(4, ElevatorState.DOWN, 10, 1))), 2},
                {new Person(5, PersonDirection.DOWN), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.EMPTY, 2, 2),
                        new Elevator(2, ElevatorState.DOWN, 6, 3),
                        new Elevator(3, ElevatorState.DOWN, 4, 2))), 2},
                {new Person(5, UP), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.UP, 7, 9),
                        new Elevator(2, ElevatorState.UP, 3, 4),
                        new Elevator(3, ElevatorState.DOWN, 10, 5))), 3},
                {new Person(5, UP), new ArrayList<>(Arrays.asList(
                        new Elevator(1, ElevatorState.UP, 7, 9),
                        new Elevator(2, ElevatorState.UP, 6, 8),
                        new Elevator(3, ElevatorState.UP, 2, 3),
                        new Elevator(4, ElevatorState.DOWN, 4, 3))), 4}
        };
    }

    @Test(dataProvider = "getOptimal")
    public void test(Person person, List<Elevator> list, int id) {
        long startTime = System.nanoTime();
        controller.setList(list);
        if (person.getDirection() == DOWN) {
            assertEquals(id, controller.down(person.getPersonFloor()));
        } else {
            assertEquals(id, controller.up(person.getPersonFloor()));
        }
        long finishTime = System.nanoTime();
        log.info(finishTime-startTime);
    }
}
